#pragma once

#include <glm/glm.hpp>
#include <string>
#include <OpenGL/gl3.h>

#include "Shader.h"

class ShaderProgram {
private:

    GLuint id;
    bool linked;

public:

    ShaderProgram();

    void attachShader(Shader &shader);

    void use();

    void setVariable(const std::string &name, const glm::mat4 &value);

    void setVariable(const std::string &name, const glm::mat3 &value);

    void setVariable(const std::string &name, const glm::vec3 &value);
};
