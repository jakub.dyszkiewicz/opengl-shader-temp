#pragma once

#include <AL/alut.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/matrix_inverse.hpp>

#define GLFW_INCLUDE_GLCOREARB

#include <GLFW/glfw3.h>

class Camera {
private:

    float fov;
    float aspectRatio;

    glm::mat4 projectionMatrix;

    glm::vec3 rotation;
    glm::vec3 position;
    glm::vec3 scale;

    glm::mat4x4 matrix;
    bool calculated;

public:

    Camera(float fov, float ratio);

    void setAspectRatio(float ratio);

    const glm::mat4 &getViewMatrix();

    const glm::mat4 &getProjectionMatrix();

    void updateListener(glm::vec3 velocity);

    void setPosition(glm::vec3 position);

    void setRotation(glm::vec3 rotation);

    glm::vec3 getPosition();

    glm::vec3 getRotation();

};