#include "Camera.h"

Camera::Camera(float fov, float ratio) {
    rotation = position = glm::vec3(0.f);
    scale = glm::vec3(1.f);
    calculated = false;
    this->fov = fov;
    aspectRatio = ratio;
    projectionMatrix = glm::perspective(fov, aspectRatio, 0.001f, 5000.f);

    setRotation(glm::vec3(90.f, 0.f, 0.f));
}

void Camera::setAspectRatio(float ratio) {
    aspectRatio = ratio;
    projectionMatrix = glm::perspective(fov, aspectRatio, 0.001f, 5000.f);
}

const glm::mat4 &Camera::getViewMatrix() {
    if (!calculated) {
        glm::mat4 identityMat = glm::mat4(1.f);
        glm::mat4 translationMat = glm::translate(identityMat, position);
        glm::mat4 rotationMatX = glm::rotate(identityMat, rotation.x, glm::vec3(1.f, 0.f, 0.f));
        glm::mat4 rotationMatY = glm::rotate(identityMat, rotation.y, glm::vec3(0.f, 1.f, 0.f));
        glm::mat4 rotationMatZ = glm::rotate(identityMat, rotation.z, glm::vec3(0.f, 0.f, 1.f));
        glm::mat4 scaleMat = glm::scale(identityMat, scale);

        matrix = translationMat * rotationMatZ * rotationMatY * rotationMatX * scaleMat;
        matrix = glm::inverse(matrix);

        calculated = true;
    }

    return matrix;
}

const glm::mat4 &Camera::getProjectionMatrix() {
    return projectionMatrix;
}

void Camera::updateListener(glm::vec3 velocity) {
    alListenerfv(AL_POSITION, &(position[0]));
    alListenerfv(AL_VELOCITY, &velocity[0]);

    float zmult = glm::sin(rotation.x);
    glm::vec3 atVec(-sin(rotation.z) * zmult, cos(rotation.z) * zmult, cos(rotation.x));
    atVec = glm::normalize(atVec);
    float ori[6] = {atVec[0], atVec[1], atVec[2], 0.0, 0.0, 1.0};
    alListenerfv(AL_ORIENTATION, &ori[0]);
}

void Camera::setPosition(glm::vec3 position) {
    this->position = position;

    calculated = false;
}

void Camera::setRotation(glm::vec3 rotation) {
    this->rotation = glm::radians(rotation);

    calculated = false;
}

glm::vec3 Camera::getPosition() {
    return position;
}

glm::vec3 Camera::getRotation() {
    return glm::degrees(rotation);
}

