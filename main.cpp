#include <iostream>
#include "Obj.h"
#include "ShaderProgram.h"
#include "Window.h"
#include "ObjImporter.h"

int main() {

    Window window(1440, 900);

    Shader vertexShader = Shader::newVertex("/Users/jakub/ClionProjects/shader/shaders/shader.vert");
    vertexShader.compile();
    Shader fragmentShader = Shader::newFragment("/Users/jakub/ClionProjects/shader/shaders/shader.frag");
    fragmentShader.compile();

    ShaderProgram shaderProgram;
    shaderProgram.attachShader(vertexShader);
    shaderProgram.attachShader(fragmentShader);

    shaderProgram.use();
    window.setShaderProgram(shaderProgram);

    ObjImporter import("/Users/jakub/ClionProjects/shader/objects/Captain_America_Avengers_Shield.obj");
    ObjImportResult importResult = import.import();
    Obj obj(importResult);
    obj.load();

    while (window.isOpen()) {
        window.draw(obj);
        window.show();
        window.clear();
    }
}
