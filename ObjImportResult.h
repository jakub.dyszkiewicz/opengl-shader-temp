#ifndef SHADER_OBJIMPORTRESULT_H
#define SHADER_OBJIMPORTRESULT_H


#include <glm/vec3.hpp>
#include <vector>

class ObjImportResult {
private:
public:
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;
    std::vector<unsigned> indexes;
    ObjImportResult(std::vector<glm::vec3> vertices, std::vector<glm::vec3> normals, std::vector<unsigned> indexes);
};


#endif
