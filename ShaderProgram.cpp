#include "ShaderProgram.h"

ShaderProgram::ShaderProgram() : id(glCreateProgram()), linked(false) {
}

void ShaderProgram::attachShader(Shader &shader) {
    glAttachShader(id, shader.id);
}

void ShaderProgram::use() {
    if (!linked) {
        glLinkProgram(id);
        linked = true;
    }

    glUseProgram(id);
}

void ShaderProgram::setVariable(const std::string &name, const glm::mat4 &value) {
    GLint var = glGetUniformLocation(id, name.c_str());
    glUniformMatrix4fv(var, 1, GL_FALSE, &value[0][0]);
}

void ShaderProgram::setVariable(const std::string &name, const glm::mat3 &value) {
    GLint var = glGetUniformLocation(id, name.c_str());
    glUniformMatrix3fv(var, 1, GL_FALSE, &value[0][0]);
}

void ShaderProgram::setVariable(const std::string &name, const glm::vec3 &value) {
    GLint var = glGetUniformLocation(id, name.c_str());
    glUniform3d(var, value.x, value.y, value.z);
}
