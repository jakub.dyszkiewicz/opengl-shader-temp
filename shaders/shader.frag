#version 330

in vec3 out_normal;
in vec3 out_position;
uniform vec3 sun_direction;

out vec4 out_color;

const vec3 color = vec3(1.0, 0.0, 0.0);

void main()
{
    // ambient - od koloru
    vec3 ambient = color;

    // diffuse - od koloru
    vec3 diffuse = color * 0.7f * max(dot(out_normal, sun_direction), 0.0f);

    // specular - Blinn-Phong
    vec3 halfwayVector = normalize(sun_direction + normalize(-out_position));
    float specTmp = max(dot(out_normal, halfwayVector), 0.0);
    vec3 sunColor = vec3(0.9, 0.5, 0.4);
    vec3 specular = sunColor * pow(specTmp, 35.0);

    out_color = vec4(0.25 * ambient + 0.75 * diffuse + 0.5 * specular, 1.0f);
}
