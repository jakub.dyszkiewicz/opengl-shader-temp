#version 330

in vec3 position;
in vec3 normal;

uniform mat4 modelview;
uniform mat4 projection;
uniform mat3 normal_matrix;

out vec3 out_normal;
out vec3 out_position;

void main()
{
//gl_Position = projection * modelview * vec4(position, 1.0f);
//    out_normal = normal;
//    out_position = position;
    gl_Position = projection * modelview * vec4(position, 1.0f);
    out_normal = normalize(normal * normal_matrix);
    vec4 vertpos = modelview * vec4(position, 1.0f);
    out_position = vec3(vertpos) / vertpos.w;
}
