#ifndef SHADER_OBJIMPORTER_H
#define SHADER_OBJIMPORTER_H

#include <vector>
#include <glm/vec3.hpp>
#include <map>
#include <fstream>
#include <sstream>
#include <string>
#include "ObjImportResult.h"

class ObjImporter {
private:
    const std::string filename;
    std::vector<glm::vec3> tempVertices;
    std::vector<glm::vec3> tempNormal;
    std::vector<std::string> tempTriangles;
    std::vector<std::string> tempIndexesVerticles;
    std::map<std::string, unsigned> tempIndexes;
public:
    ObjImporter(std::string filename);

    ObjImportResult import();

    ObjImportResult getResult();
};

#endif