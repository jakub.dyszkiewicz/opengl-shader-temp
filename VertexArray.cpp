#include "VertexArray.h"

VertexArray::VertexArray() {
    glGenVertexArrays(1, &id);
}

void VertexArray::bindVerticesBuffer(std::vector<glm::vec3> &data) {
    bindBuffer(data, "position");
}

void VertexArray::bindNormalBuffer(std::vector<glm::vec3> &data) {
    bindBuffer(data, "normal");
}

void VertexArray::bindBuffer(std::vector<glm::vec3> &data, std::string attribName) {
    glBindVertexArray(id);

    GLuint buffer;
    glGenBuffers(1, &buffer);
    glBindBuffer(GL_ARRAY_BUFFER, buffer);
    glBufferData(GL_ARRAY_BUFFER, data.size() * sizeof(glm::vec3), &data[0], GL_STATIC_DRAW);

    GLint currentProgramId;
    glGetIntegerv(GL_CURRENT_PROGRAM, &currentProgramId);
    GLuint attrib = (GLuint) glGetAttribLocation((GLuint) currentProgramId, attribName.c_str());
    glEnableVertexAttribArray(attrib);
    glVertexAttribPointer(attrib, 3, GL_FLOAT, GL_FALSE, 0, 0);
}

void VertexArray::bindBuffer(std::vector<unsigned> &data) {
    glBindVertexArray(id);

    GLuint buffer;
    glGenBuffers(1, &buffer);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, data.size() * sizeof(unsigned), &data[0], GL_STATIC_DRAW);
}

void VertexArray::bind() {
    glBindVertexArray(id);
}
