#pragma once

#include <glm/glm.hpp>

#include <string>
#include <vector>
#include <OpenGL/gl3.h>

class VertexArray {
private:

    GLuint id;

    void bindBuffer(std::vector<glm::vec3> &data, std::string attribName);

public:

    VertexArray();

    void bindVerticesBuffer(std::vector<glm::vec3> &data);

    void bindNormalBuffer(std::vector<glm::vec3> &data);

    void bindBuffer(std::vector<unsigned> &data);

    void bind();
};
