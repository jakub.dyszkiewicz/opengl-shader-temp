#include <cassert>
#include "Shader.h"

Shader::Shader(GLenum shaderType, const std::string &filename) : id(glCreateShader(shaderType)) {
    load(filename);
}

void Shader::load(const std::string &filename) {
    std::ifstream file(filename);
    assert(file.good());
    while (file.good()) {
        std::string line;
        std::getline(file, line);
        source += line + '\n';
    }
}

void Shader::compile() {
    const char *data = source.c_str();

    glShaderSource(id, 1, &data, NULL);
    glCompileShader(id);

    if (getErrors() != "") {
        throw std::runtime_error(getErrors());
    }
}

const std::string Shader::getErrors() {
    std::string result;

    GLint compileStatus = 0;
    glGetShaderiv(id, GL_COMPILE_STATUS, &compileStatus);
    if (!compileStatus) {
        int length = 0;
        glGetShaderiv(id, GL_INFO_LOG_LENGTH, &length);
        if (length) {
            GLchar *log = (GLchar *) malloc((size_t) length);
            glGetShaderInfoLog(id, length, NULL, log);
            result = log;
            free(log);
        }
    }
    return result;
}

Shader Shader::newFragment(const std::string &filename) {
    return Shader(GL_FRAGMENT_SHADER, filename);
}

Shader Shader::newVertex(const std::string &filename) {
    return Shader(GL_VERTEX_SHADER, filename);
}



