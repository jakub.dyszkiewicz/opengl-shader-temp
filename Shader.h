#pragma once

#include <iostream>
#include <fstream>
#include <OpenGL/gl3.h>

class Shader {
private:
    Shader(GLenum shaderType, const std::string &filename);
    std::string source;

public:
    const GLuint id;
    static Shader newFragment(const std::string &filename);

    static Shader newVertex(const std::string &filename);

    void load(const std::string &filename);

    void compile();

    const std::string getErrors();
};
