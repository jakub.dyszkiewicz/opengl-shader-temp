#include "ObjImporter.h"

std::vector<std::string> split(const std::string &s, char delim) {
    std::vector<std::string> elems;
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        elems.push_back(item);
    }
    return elems;
}

ObjImportResult ObjImporter::import() {
    std::ifstream file(filename);
    unsigned faceIndex = 0;

    while (file.good()) {
        std::string line;
        std::getline(file, line);

        auto splited = split(line, ' ');

        if (splited.size() <= 0)
            continue;

        if (splited[0] == "v") {
            tempVertices.push_back(glm::vec3(std::stof(splited[1]), std::stof(splited[2]), std::stof(splited[3])));
        }

        if (splited[0] == "vn") {
            tempNormal.push_back(glm::vec3(std::stof(splited[1]), std::stof(splited[2]), std::stof(splited[3])));
        }

        if (splited[0] == "f" && splited.size() == 4) {
            tempTriangles.push_back(line);

            for (unsigned i = 1; i < 4; i++) {
                if (tempIndexes.count(splited[i]) == 0) {
                    tempIndexes[splited[i]] = faceIndex++;
                    tempIndexesVerticles.push_back(splited[i]);
                }
            }
        }
    }

    return getResult();

}

ObjImportResult ObjImporter::getResult() {
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normalVertices;
    std::vector<unsigned> indexes;

    for (unsigned i = 0; i < tempIndexesVerticles.size(); i++) {
        auto attributes = split(tempIndexesVerticles[i], '/');

        if (attributes[0] != "") {
            vertices.push_back(tempVertices[stoi(attributes[0]) - 1]);
        }

        if (attributes[2] != "") {
            normalVertices.push_back(tempNormal[stoi(attributes[2]) - 1]);
        }
    }

    for (unsigned i = 0; i < tempTriangles.size(); i++) {
        auto parts = split(tempTriangles[i], ' ');

        for (unsigned j = 1; j < 4; j++) {
            indexes.push_back(tempIndexes[parts[j]]);
        }
    }

    return ObjImportResult(vertices, normalVertices, indexes);
}

ObjImporter::ObjImporter(std::string filename) : filename(filename) {
}