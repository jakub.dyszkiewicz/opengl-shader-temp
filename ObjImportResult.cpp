#include "ObjImportResult.h"

ObjImportResult::ObjImportResult(std::vector<glm::vec3> vertices, std::vector<glm::vec3> normals,
                                 std::vector<unsigned> indexes) : vertices(vertices), normals(normals), indexes(indexes) {

}

