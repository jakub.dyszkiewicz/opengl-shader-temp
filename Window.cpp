#include "Window.h"

Window::Window(unsigned width, unsigned height) : camera(45.f, width / (float) height),
                                                  mouseX(10, 0), mouseY(10, 0) {
    lastTime = 0;
    glfwSetErrorCallback(errorCallback);

    if (!glfwInit()) {
        printf("Error: Could not initiate GLFW\n");
        exit(EXIT_FAILURE);
    }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    window = glfwCreateWindow(width, height, "asd", NULL, NULL);

    if (!window) {
        printf("Error: Could not create a GLFW window\n");
        glfwTerminate();
        exit(EXIT_FAILURE);
    }

    glfwSetKeyCallback(window, keyCallback);
    glfwMakeContextCurrent(window);
    glfwSwapInterval(1);
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    glfwSetCursorPos(window, 0, 0);

    int major, minor, rev;
    major = glfwGetWindowAttrib(window, GLFW_CONTEXT_VERSION_MAJOR);
    minor = glfwGetWindowAttrib(window, GLFW_CONTEXT_VERSION_MINOR);
    rev = glfwGetWindowAttrib(window, GLFW_CONTEXT_REVISION);
    printf("OpenGL version recieved: %d.%d.%d\n", major, minor, rev);
    printf("Supported OpenGL is %s\n", (const char *) glGetString(GL_VERSION));
    printf("Supported GLSL is %s\n", (const char *) glGetString(GL_SHADING_LANGUAGE_VERSION));

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);

    glfwGetFramebufferSize(window, &(this->width), &(this->height));


    alutInit(NULL, 0);
    alGetError();
}

Window::~Window() {
    glfwDestroyWindow(window);
    glfwTerminate();

    alutExit();
}

bool Window::isOpen() {
    return !glfwWindowShouldClose(window);
}

void Window::clear() {
    int width, height;
    glfwGetFramebufferSize(window, &width, &height);

    if (width / (float) (height) != width / (float) height) {
        camera.setAspectRatio(width / (float) height);
        this->width = width;
        this->height = height;

        glViewport(0, 0, width, height);
    }

    glClearColor(0.5f, 1.0f, 0.4f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glm::vec3 rotvec = camera.getRotation();

    glfwGetCursorPos(window, &(mouseX[0]), &(mouseY[0]));

    for (int i = 0; i < mouseX.size(); i++) {
        float mult = pow(0.5, i + 1) / 5.f;
        rotvec.z -= mouseX[i] * mult;
        rotvec.x -= mouseY[i] * mult;
    }

    camera.setRotation(rotvec);
    glfwSetCursorPos(window, 0, 0);

    for (int i = mouseX.size() - 1; i >= 0; i--) {
        mouseX[i + 1] = mouseX[i];
        mouseY[i + 1] = mouseY[i];
    }

    auto pos = camera.getPosition();

    double sinDelta = glm::sin(glm::radians(camera.getRotation().z)) * delta * 10.f;
    double cosDelta = glm::cos(glm::radians(camera.getRotation().z)) * delta * 10.f;
    double zDelta = glm::cos(glm::radians(camera.getRotation().x)) * delta * 10.f;
    double zMult = glm::sin(glm::radians(camera.getRotation().x));

    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) {
        pos.x -= sinDelta * zMult;
        pos.y += cosDelta * zMult;
        pos.z -= zDelta;
    }

    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) {
        pos.x += sinDelta * zMult;
        pos.y -= cosDelta * zMult;
        pos.z += zDelta;
    }

    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) {
        pos.x -= cosDelta;
        pos.y -= sinDelta;
    }

    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) {
        pos.x += cosDelta;
        pos.y += sinDelta;
    }

    camera.setPosition(pos);

    double xVel = (pos.x - camera.getPosition().x) / delta;
    double yVel = (pos.y - camera.getPosition().y) / delta;
    double zVel = (pos.z - camera.getPosition().z) / delta;
    camera.updateListener(glm::vec3(xVel, yVel, zVel));
}

void Window::show() {
    glfwSwapBuffers(window);

    while (glfwGetTime() - lastTime < 1 / 60.0);
    delta = glfwGetTime() - lastTime;
    lastTime = glfwGetTime();

    glfwPollEvents();
}

void Window::errorCallback(int error, const char *description) {
    printf("Error %X: %s\n", error, description);
}

void Window::keyCallback(GLFWwindow *window, int key, int scancode, int action, int mods) {
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GL_TRUE);
}

void Window::draw(Obj &obj) {
    glm::mat4 modelView = camera.getViewMatrix();
    glm::mat3 normalMat = glm::inverseTranspose(glm::mat3(camera.getViewMatrix()));
    glm::vec4 sunDirVec4 = glm::normalize(camera.getViewMatrix() * glm::vec4(0.0f, 0.0f, 1.0f, 0.0f));
    glm::vec3 sunDirection(sunDirVec4.x, sunDirVec4.y, sunDirVec4.z);

    program->setVariable("normal_matrix", normalMat);
    program->setVariable("modelview", modelView);
    program->setVariable("projection", camera.getProjectionMatrix());
    program->setVariable("sun_direction", sunDirection);

    obj.bind();

    glDrawElements(GL_TRIANGLES, obj.getIndicesCount(), GL_UNSIGNED_INT, 0);
}


void Window::setShaderProgram(ShaderProgram &program) {
    this->program = &program;
}
