#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <string>
#include <fstream>
#include <vector>
#include <map>

#include "VertexArray.h"
#include "ObjImportResult.h"

class Obj {
private:

    VertexArray vao;
    GLuint nIndexes;
    ObjImportResult importResult;

public:

    Obj(const ObjImportResult importResult);

    void bind();

    void load();

    GLuint getIndicesCount();
};