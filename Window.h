#pragma once

#define GLFW_INCLUDE_GLCOREARB

#include <GLFW/glfw3.h>
#include <OpenGL/gl3.h>

#include "Camera.h"
#include "Obj.h"
#include "ShaderProgram.h"

#include <iostream>
#include <vector>

class Window {
private:

    friend GLFWAPI GLFWkeyfun glfwSetKeyCallback(GLFWwindow *window, GLFWkeyfun cbfun);

    friend GLFWAPI GLFWerrorfun glfwSetErrorCallback(GLFWerrorfun cbfun);

    static void errorCallback(int error, const char *description);

    static void keyCallback(GLFWwindow *window, int key, int scancode, int action, int mods);

    int width, height;

    GLFWwindow *window;
    ShaderProgram *program;
    Camera camera;
    double delta;
    double lastTime;
    std::vector<double> mouseX;
    std::vector<double> mouseY;

public:

    Window(unsigned width, unsigned height);

    ~Window();

    bool isOpen();

    void clear();

    void draw(Obj &obj);

    void show();

    void setShaderProgram(ShaderProgram &program);
};