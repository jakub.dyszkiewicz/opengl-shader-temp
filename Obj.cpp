#include <sstream>
#include "Obj.h"

void Obj::bind() {
    vao.bind();
}

void Obj::load() {
    vao.bindVerticesBuffer(importResult.vertices);
    vao.bindNormalBuffer(importResult.normals);
    vao.bindBuffer(importResult.indexes);
    nIndexes = (GLuint) importResult.indexes.size();
}

GLuint Obj::getIndicesCount() {
    return nIndexes;
}

Obj::Obj(ObjImportResult importResult) : importResult(importResult) {
}

